## v2.0.6 (2024-10-15)

### Changed

- update makefile
- update pre commit
- Update foundry scripts
- Update makefile and foundry scripts
- Update makefile and foundry scripts
- Update repository list

## v2.0.5 (2024-10-13)

### Changed

- Update clone repository script

### Tools

- Update sem ver config

## v2.0.4 (2024-10-10)

### Changed

- Add keybinding scraper script

### Tools

- Update pre commit and poetry config 10.10.24

## v2.0.3 (2024-10-10)

### Changed

- Add bash script creator - lazyness
- update workspace installation json, add yaml

### Tools

- Update pre commit and poetry config 05.08.24

## v2.0.2 (2024-04-16)

### Changed

- Rename workbench

### Tools

- Poetry fix
- Update pre commit hooks and pyproject config

## v2.0.1 (2024-03-09)

### Patch

- poetry update to lastest
- Refactor update git repos script to ignore given directories
- Apply black suggestions
- Update pre commit configuration
- Update poetry

## v2.0.0 (2024-01-17)

### Breaking Change

- Refactor structure to match poetry requirements, update versions

## v1.1.2 (2023-11-12)

### Patch

- Update python versions
- Add rest client, similar to already existent one

## v1.1.1 (2023-10-29)

### Fix

- Fix pyproject bump

## v1.1.0 (2023-10-29)

### Feature

- Migrate to commitizen with local push and bump

## v1.0.0 (2023-10-28)

### Patch

- Migrate configuration to poetry
- Update decorator script with iterm, move files to examples

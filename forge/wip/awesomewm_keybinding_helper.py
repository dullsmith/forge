#!/usr/bin/env python3

import re
import sys

## TODO: This was mostly generated by gpt, needs to be checked

# Define the regex pattern to match keybindings
pattern = re.compile(
    r'awful\.key\s*\(\s*\{([^}]*)}\s*,\s*"([^"]+)"\s*,\s*.*?\{\s*description\s*=\s*"([^"]+)"',
    re.DOTALL,
)

# Define a regex pattern for keygroup bindings
keygroup_pattern = re.compile(
    r'awful\.key\s*\{\s*modifiers\s*=\s*\{([^}]*)}\s*,\s*keygroup\s*=\s*"([^"]+)"\s*,\s*description\s*=\s*"([^"]+)"',
    re.DOTALL,
)


def extract_keybindings(file_content):
    # Find all keybindings with modifiers, keys, and descriptions
    keybindings = re.findall(pattern, file_content)

    # Find all keybindings that use keygroups
    keygroup_bindings = re.findall(keygroup_pattern, file_content)

    # Combine results into a single list (modifiers, key, description)
    bindings = [(modifiers, key, desc) for modifiers, key, desc in keybindings]
    keygroup_bindings = [
        (modifiers, keygroup, desc) for modifiers, keygroup, desc in keygroup_bindings
    ]

    return bindings + keygroup_bindings


def format_markdown_table(bindings):
    # Create Markdown table header
    markdown_table = "| Keys| Description |\n"
    markdown_table += "|-----|----|\n"

    # Add each binding as a row in the Markdown table
    for modifiers, key, description in bindings:
        # Clean and format modifiers
        modifiers_formatted = modifiers.replace('"', "").replace(" ", "").split(",")
        # Wrap each modifier and key in <kbd> tags
        formatted_combination = " + ".join(
            [f"<kbd>{modifier}</kbd>" for modifier in modifiers_formatted]
            + [f"<kbd>{key}</kbd>"]
        )

        # Add row to the Markdown table
        markdown_table += f"| {formatted_combination} | {description} |\n"

    return markdown_table


def main(file_path):
    # Read the file content
    try:
        with open(file_path, "r") as f:
            file_content = f.read()
    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
        return

    # Extract keybindings from the file content
    bindings = extract_keybindings(file_content)

    # Format the keybindings into a Markdown table
    markdown_table = format_markdown_table(bindings)

    # Print the Markdown table
    print(markdown_table)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python extract_keybindings.py <path_to_lua_file>")
    else:
        file_path = sys.argv[1]
        main(file_path)

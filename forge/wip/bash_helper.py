import re

import pyfiglet


# Generate a figlet (ASCII art) for the script header
def figlet(filename: str):
    filename = str.capitalize(filename)
    figlet_header = pyfiglet.figlet_format(filename, font="larry3d")
    figlet_comment = figlet_header.replace("\n", "\n# ")
    return figlet_comment


# Function to create the bash script
def create_bash_script(filename):
    figlet_header = figlet(filename)

    # Basic content of the bash script
    bash_script_content = f"""

#!/bin/bash

# This script was automatically generated

# {figlet(filename)}

# Enable strict mode:
# -e: Exit immediately if a command exits with a non-zero status
# -u: Treat unset variables as an error
# -o pipefail: Ensure that any failure in a pipeline causes the script to fail
set -euo pipefail

# Define colors for output using tput
GREEN=$(tput setaf 2)
RESET=$(tput sgr0)

# Function to print an info message
function print_info() {{
    echo -e "${{GREEN}}$1${{RESET}}"
}}

# Helper function to show available commands
function show_help() {{
    print_info "Available commands:"
    echo "  start  - Start the process"
    echo "  stop   - Stop the process"
    echo "  help   - Display this help message"
}}

# Handle commands
case "$1" in
    start)
        echo "Starting the process..."
        # Add your start logic here
        ;;
    stop)
        echo "Stopping the process..."
        # Add your stop logic here
        ;;
    help|*)
        show_help
        ;;
esac
"""

    # Use the input name directly for the filename
    file_name = f"{script_name}.sh"

    # Write the bash script content to the file
    with open(file_name, "w") as file:
        file.write(bash_script_content)

    # Output instructions
    print(f"Bash script '{file_name}' has been created.")
    print(f"Run the following command to make it an executable:\nchmod +x {file_name}")


if __name__ == "__main__":
    # Accept user input for the name of the script
    script_name = input("Enter the name for the bash script: ").strip()

    # Ensure the filename name doesn't contain invalid characters
    script_name = re.sub("[^A-Za-z0-9]+", "-", script_name)

    create_bash_script(script_name)

# Ideas to consider and refresh git folder update
#
# 1. Handle repositories where main or master is not default, consider ignoring them
# 2. Add option verbose to not clutter so much
# 3. Consider aggregating valuable logs and print them only in the end
# 4. Check visual options on python, like gum
#   a. https://github.com/idobarel/gum-python/blob/main/gum/utils.py
#   b. Install gum and wrap the usecases like with git?
# 5. Check out state of the art
#   a. https://github.com/asottile/all-repos/blob/main/all_repos/sed.py
#   b. https://github.com/gitpython-developers/GitPython

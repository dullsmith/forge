#!/usr/bin/env python3
import argparse
import random
import string

__version__ = "0.0.1"


def define_arguments():
    """
    Define allowed arguments.
    Refer to https://docs.python.org/3/library/argparse.html for documentation
    """
    parser = argparse.ArgumentParser("Password generator")
    parser.add_argument(
        "-v", "--version", action="version", version="%(prog)s " + __version__
    )
    parser.add_argument(
        "-l",
        "--length",
        type=int,
        default=15,
        help="Password length",
    )
    parser.add_argument(
        "-lc",
        "--lower",
        action="store_true",
        default=False,
        help="Mixed case",
    )
    parser.add_argument(
        "-s", "--special", action="store_true", default=False, help="Special characters"
    )
    return parser


def parse_arguments(parser: argparse.ArgumentParser):
    """The handling of parameters from the command line"""

    args, unknowns = parser.parse_known_args()

    if type(args) == argparse.Namespace:
        args = vars(args)
    print(f"Config: {args}")
    return args


def generate(length: int, lower_case: bool, special_characters: bool):
    characters = []
    if special_characters is True:
        special_character_length = random.randrange(1, length // 2)
        characters = random.choices("!@#$%^&*", k=special_character_length)
        length = length - special_character_length

    if lower_case is True:
        letters_and_numbers = random.choices(
            string.ascii_lowercase + string.digits, k=length
        )
    else:
        letters_and_numbers = random.choices(
            string.ascii_letters + string.digits, k=length
        )

    characters_list = list(characters + letters_and_numbers)
    random.shuffle(characters_list)
    return str("".join(characters_list))


arguments = define_arguments()
parsed_arguments = parse_arguments(arguments)
length_arg = parsed_arguments.get("length")
lower_case_arg = parsed_arguments.get("lower")
special_arg = parsed_arguments.get("special")

password = generate(length_arg, lower_case_arg, special_arg)
print(f"Password: {password}")

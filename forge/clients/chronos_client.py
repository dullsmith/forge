""" Chronos connector client"""

""" https://mesos.github.io/chronos/docs/api.html """

import dataclasses
import datetime
import json
import os
import requests

from forge.settings import DATA_DIR


@dataclasses.dataclass
class ChronosClient:
    url: str

    def find_job(self, job_name: str):
        response = requests.get(f"{self.url}/scheduler/jobs/search?name={job_name}")
        assert response.status_code == 200
        return response.json()

    def find_all_jobs(self):
        response = requests.get(f"{self.url}/scheduler/jobs")
        assert response.status_code == 200
        return response.json()

    def find_jobs_per_owner(self, team: str):
        team_jobs = {}
        response = self.find_all_jobs()
        for job in response:
            if job["ownerName"].casefold() == team.casefold():
                team_jobs[job["name"]] = job
        return team_jobs

    def deploy_job(self, file, auth):
        response = requests.post(f"{self.url}/scheduler/iso8601", json=file, auth=auth)
        assert response.status_code == 201
        return response

    def save_job_config(self, job: str):
        response = self.find_job(job)
        directory = os.path.join(DATA_DIR, f"{datetime.date.today()}")

        if not os.path.isdir(directory):
            os.mkdir(directory)

        with open(f"{directory}/{job}.json", "w") as f:
            json.dump(response[0], f)


env = ChronosClient(url="https://localhost:8080")

authorization = ("username", "password")

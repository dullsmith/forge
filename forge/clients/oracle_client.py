"""OracleDB client connector"""

""" https://python-oracledb.readthedocs.io/en/latest/ """

import oracledb
import sys


class OracleDB:
    user: str
    password: str
    tns: str
    conn = oracledb.Connection

    def __init__(self, user, password, tns):
        self.user = user
        self.password = password
        self.tns = tns
        self.conn = self.connect()

    def connect(self):
        try:
            connection = oracledb.connect(
                user=self.user, password=self.password, dsn=self.tns
            )
            print(
                f"Oracle connection for {self.tns} established with oracle driver: {connection.version}"
            )
        except oracledb.Error as error:
            print(error)
            print(
                """
                    Connection to oracledb failed. Script was interrupted.
                    Common causes for this failure are oracle credentials are wrong or missing, or failure of VPN connection.
                    """
            )
            sys.exit()
        return connection

    def query(self, query: str):
        try:
            cursor = self.conn.cursor()

            cursor.execute(query)
            return cursor

        except oracledb.DatabaseError as err:
            print(f"ERROR: {err}")


oracle_env = OracleDB("USERNAME", "PASSWORD", "TNS")

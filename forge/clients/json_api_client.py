import dataclasses
import typing

import requests

JSON_API_URL = "https://localhost:8080"
JSON_API_AUTH = ("username", "password")


@dataclasses.dataclass
class JsonApiClient:
    url: str
    auth: typing.Tuple

    def post_object(self, json_object, path):
        r = requests.post(f"{self.url}/{path}", json=json_object, auth=self.auth)
        return r

    def post_identity(self, path):
        r = requests.post(f"{self.url}/{path}", auth=self.auth)
        return r

    def post_object_with_identity(self, identity, json_object, path):
        assert "/" not in str(identity)
        assert "?" not in str(identity)
        r = requests.post(
            f"{self.url}/{path}/{identity}", json=json_object, auth=self.auth
        )
        return r

    def get_object_with_identity(self, identity, path):
        assert "/" not in str(identity)
        assert "?" not in str(identity)
        r = requests.get(f"{self.url}/{path}/{identity}")
        return r


env_client_api = JsonApiClient(url=JSON_API_URL, auth=JSON_API_AUTH)

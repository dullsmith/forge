""" Client for keycloak"""

""" https://wjw465150.gitbooks.io/keycloak-documentation/content/server_admin/topics/sso-protocols/oidc.html """

import dataclasses

import requests


@dataclasses.dataclass
class KeycloakClient:
    keycloak_token_url: str
    keycloak_client_id: str
    keycloak_client_secret: str
    xcaller: str

    def fetch_token(self):
        kc_params = {"grant_type": "client_credentials"}
        kc_token_response = requests.post(
            self.keycloak_token_url,
            data=kc_params,
            auth=requests.auth.HTTPBasicAuth(
                username=self.keycloak_client_id, password=self.keycloak_client_secret
            ),
        )
        return kc_token_response.json()["access_token"]

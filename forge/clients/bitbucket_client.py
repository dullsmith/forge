""" Bitbucket connector client """

""" https://atlassian-python-api.readthedocs.io/bitbucket.html """
""" Requirements: Bitbucket token, .env settings"""

import atlassian
import os
from dotenv import dotenv_values


class BitbucketClient:
    def __init__(self):
        config = dotenv_values(os.path.join(ROOT_DIR, ".env"))
        self.bitbucket = atlassian.Bitbucket(
            "https://src.private.joke.net/", token=config["BB"]
        )

    def projects(self):
        return self.bitbucket.project_list()

    def get_project(self, key):
        return self.bitbucket.project(key)

    def get_repos(self, key):
        return self.bitbucket.repo_all_list(key)


# Find Projects
print(str(list(BitbucketClient().projects())))

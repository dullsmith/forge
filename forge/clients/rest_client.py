import dataclasses

import requests


@dataclasses.dataclass
class Rest(object):
    def __init__(self, base_url):
        self.base_url = base_url

    def get(self, endpoint):
        response = requests.get(self._build_endpoint(endpoint), endpoint)
        return response.json(), response.status_code

    def put(self, endpoint, payload):
        response = requests.put(self._build_endpoint(endpoint), payload)
        return response.json(), response.status_code

    def post(self, endpoint, payload):
        response = requests.post(url=self._build_endpoint(endpoint), json=payload)
        return response.json(), response.status_code

    def _build_endpoint(self, endpoint):
        return "{}/{}".format(self.base_url, endpoint)

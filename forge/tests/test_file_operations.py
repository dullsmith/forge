from unittest.mock import patch

import pytest
from dotenv import load_dotenv

from forge.settings import MODULE_DIR
from forge.review.files import filepath, get_files, line_list, get_file_data_list

load_dotenv()

SRC_PATH = MODULE_DIR
RESOURCE_FOLDER = "/tests/resources/"
DUMMY_CSV = f"{SRC_PATH}{RESOURCE_FOLDER}dummy.csv"
EMPTY_CSV = f"{SRC_PATH}{RESOURCE_FOLDER}empty.csv"
FILE = ".gitignore"


class TestFileOperations:
    def test_get_single_filename(self):
        path = filepath(FILE, False)
        assert path == [f"{SRC_PATH}/.data/.gitignore"]

    def test_get_single_file(self):
        file = get_files(FILE, 1)
        assert file == f"{SRC_PATH}/.data/.gitignore"

    def test_failure_get_multiple_files(self):
        with pytest.raises(SystemExit):
            get_files(FILE, 2)

    def test_get_multiple_files(self):
        with patch("forge.review.files.filepath", return_value=[DUMMY_CSV, EMPTY_CSV]):
            files = get_files(".gitkeep.csv", None)
            assert files == [DUMMY_CSV, EMPTY_CSV]

    def test_line_list(self):
        lines = line_list(DUMMY_CSV)
        assert len(lines) == 2
        assert lines[1] == "1,2,3\n"

    def test_empty_line_list(self):
        lines = line_list(EMPTY_CSV)
        assert len(lines) == 0

    def test_get_file_data_list(self):
        with patch("forge.review.files.get_files", return_value=DUMMY_CSV):
            lines = get_file_data_list(DUMMY_CSV)
            assert len(lines) == 2
            assert lines[1] == "1,2,3\n"

    def test_get_file_with_empty_data_list(self):
        with patch("forge.review.files.get_files", return_value=EMPTY_CSV):
            lines = get_file_data_list(EMPTY_CSV)
            assert len(lines) == 0

    def test_get_empty_file(self):
        with pytest.raises(SystemExit):
            get_file_data_list("FILE.file")

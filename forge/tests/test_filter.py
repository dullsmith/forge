from forge.review.filter import find_integer_after_pattern


def test_find_integer_after_pattern():
    test_line = "some text of an weird 4231 ID: 3078 - 1234"
    value_found = find_integer_after_pattern(test_line, "ID:")
    assert value_found == "3078"


def test_pattern_not_found_returns_none():
    test_line = "some text of an weird 4231 ID: 3078 - 1234"
    some_value = find_integer_after_pattern(test_line, "DONT")
    assert some_value is None


def test_integers_not_found_returns_none():
    test_line = "some text of without any integers on it"
    some_value = find_integer_after_pattern(test_line, "without")
    assert some_value is None

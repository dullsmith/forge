""" Operations on directories"""

import os


def locate_directory(path: str):
    """Check if the given path is a directory"""
    if not os.path.isdir(path):
        # Lazily I decided to use the file error,
        # because I could not find a better one and unix considers directories as files
        raise FileNotFoundError(f"No directory was found on: {path}")


def create_dir(path: str):
    """Create directory in the given path"""
    if not os.path.isdir(path):
        os.mkdir(path)
    else:
        print(f"WARNING: Directory already present on {path}, reusing it!")

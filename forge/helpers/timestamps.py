""" Find timestamps in given string(s)"""

import re
from datetime import datetime

"""
Example of regex and formatter:
time_regex = r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}"
formatter = "%Y-%m-%dT%H:%M:%S"
Or
time_regex = r"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}"
formatter = "%Y-%m-%d %H:%M:%S"
"""


def find_timestamps(regex: str, formatter: str, data: list) -> list:
    """
    Given a string list, find all timestamps matching a regex
    Return them according to given formatter
    """
    timestamps = []
    for line in data:
        timestamp = get_timestamp(regex, formatter, line)
        if timestamp:
            timestamps.append(timestamp)
    return timestamps


def get_timestamp(regex: str, formatter: str, line: str) -> datetime:
    """
    Given a string find all timestamps matching a regex
    Return them according to given formatter
    """
    match = re.search(regex, line)
    if match is not None:
        return datetime.strptime(match.group(), formatter)

""" Operations on jsons"""

import json


def get_json_object(filename: str):
    """Return a json object from file"""
    file = open(filename)
    return json.load(file)

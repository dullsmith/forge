"""Operations on files"""

import os


def locate_file(path):
    """Check if the given path is an existent file"""
    if not os.path.isfile(path):
        raise FileNotFoundError(f"No file was found on path: {path}")

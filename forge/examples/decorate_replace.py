#!/usr/bin/env python3

# This script expects the name of a decorator/resources json file
# It will load the colorscheme from the given file
# load all files in the template directory and replace the {{handlebars}}
# This will regenerate the files in the output folder - that are being read
# by the applications that use colors - eg. xresources, rofi, alacritty, etc

import glob
import json
import os
import sys

import json
import os

import sys

__version__ = "2.1.0"

from argparse import ArgumentParser, Namespace
from typing import Dict, Any

parser = ArgumentParser()


def define_arguments():
    """
    Define allowed arguments.
    Refer to https://docs.python.org/3/library/argparse.html for documentation
    """
    parser.add_argument(
        "-v", "--version", action="version", version="%(prog)s " + __version__
    )
    parser.add_argument(
        "-s",
        "--source",
        type=str,
        help="Path to JSON configuration file",
        required=True,
    )


HOME = os.getenv("HOME")
XDG_CONFIG_DIR = os.getenv("XDG_CONFIG_HOME", os.path.join(HOME, ".cache"))

if len(sys.argv) != 2:
    sys.exit(
        f"script, source and target filename argument were expected, instead {len(sys.argv)} arguments were found"
    )

SOURCE_FILE = f"{XDG_CONFIG_DIR}/decorator/resources/{sys.argv[1]}.json"
if os.path.exists(SOURCE_FILE) is False:
    sys.exit(f"Color schema file {SOURCE_FILE} was not found!")

TEMPLATE_DIR = f"{XDG_CONFIG_DIR}/decorator/templates"
if os.path.exists(TEMPLATE_DIR) is False:
    sys.exit(
        f"Template folder does not exist, please add templates to /decorator/templates"
    )
if not os.listdir(TEMPLATE_DIR):
    sys.exit(f"Template directory is empty")

OUTPUT_DIR = f"{XDG_CONFIG_DIR}/decorator/output"
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

template_files = glob.glob(f"{TEMPLATE_DIR}/*")


def get_colorschema() -> json:
    with open(SOURCE_FILE) as jfile:
        return json.load(jfile)


def get_lines(filename: str) -> list:
    with open(filename, "rt") as data:
        return list(data)


def replace_template(line: str, json: dict) -> str:
    for key, value in json.items():
        ## fstring requires double {{ }} for representing {}
        ## because of its use of {} for arguments
        HANDLEBARS = f"{{{{{key}}}}}"
        if HANDLEBARS in line:
            new_line = line.replace(HANDLEBARS, value)
            return new_line
    return line


def replace_colors(lines: list, json: dict) -> list:
    new_lines = []
    for line in lines:
        res = replace_template(line, json)
        new_lines.append(res)
    return new_lines


def write_new_file(filepath: str, lines: list):
    filename = os.path.basename(filepath)
    schema = open(f"{OUTPUT_DIR}/{filename}", "w", newline="", encoding="utf-8")
    for entry in lines:
        schema.write(entry)


for path in template_files:
    file_lines = get_lines(path)
    new_file_lines = replace_colors(file_lines, get_colorschema())
    write_new_file(path, new_file_lines)

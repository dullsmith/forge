#!/usr/bin/env python3

"""
Create scratch note with relevant fields:
- Id
- Date
- Content,
- Todo
- References
- Tags
"""

import os
import sys
from argparse import ArgumentParser, Namespace
from datetime import datetime
from typing import Dict, Any

__version__ = "3.1"

parser = ArgumentParser()


def define_arguments():
    """
    Define allowed arguments.
    Refer to https://docs.python.org/3/library/argparse.html for documentation
    """
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="Zettlekasten script (zk) v" + __version__,
    )
    parser.add_argument(
        "-f",
        "--filename",
        type=str,
        help="Filename - should be topical, it will be used for file header",
        required=True,
    )


def get_arguments() -> Dict[str, Any]:
    """The handling of parameters from the command line"""

    args, unknowns = parser.parse_known_args()

    execution_date = datetime.now()
    args.full_date = execution_date.strftime("%d-%m-%Y %H:%M:%S")
    args.timestamp = int(datetime.timestamp(execution_date))
    args.title = args.filename.replace("-", " ").title()

    if type(args) == Namespace:
        args = vars(args)

    print(f"Arguments: {args}")
    return args


def create_scratch_lines(
    uniqueness_headers: dict, markdown_headers: list, description: str
) -> list:
    """Generate line list according to input arguments"""
    scratch_lines = [f"# {description}\n\n"]
    for item in uniqueness_headers:
        scratch_lines.append(f"{item}: {uniqueness_headers[item]}\n\n")
    for header in markdown_headers:
        scratch_lines.append(f"# {header}\n\n")
    return scratch_lines


def create_filepath():
    """Generate output directory, validate if output folder exists"""
    directory = f"{HOME}/scratches"
    if not os.path.exists(directory):
        sys.exit(f"Output directory not found: {directory}")
    filepath = f"{directory}/{arguments['filename']}-{arguments['timestamp']}.md"
    return filepath


def create_new_note(path: str, lines: list):
    """Create file with given argument lines"""
    try:
        with open(path, "x", newline="", encoding="utf-8") as note:
            note.writelines(lines)
        print(f"File create in: {path}")
    finally:
        note.close()


HOME = os.path.dirname(os.path.dirname(__file__))

define_arguments()
arguments = get_arguments()

markdown_headers = ["Content", "Todo", "References", "Tags"]

uniqueness_headers = {
    "id": str(arguments["timestamp"]),
    "creation date": str(arguments["full_date"]),
}

create_new_note(
    create_filepath(),
    create_scratch_lines(uniqueness_headers, markdown_headers, arguments["title"]),
)

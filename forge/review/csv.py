import csv


def write_single_column_to_csv(header: str, entries: list, filename: str):
    # print(f"header: {header[:10]}, entries {entries[:10]}, filename {filename}")
    entry_list = []
    for entry in entries:
        entry_list.append([entry])
    write_to_csv([header], entry_list, filename)


def write_to_csv(header: list, entries: list, filename: str):
    # print(f"header: {header[:10]}, entries {entries[:10]}, filename {filename}")
    with open(filename, "x", newline="", encoding="utf-8") as file:
        output = csv.writer(file)
        output.writerow(header)
        output.writerows(entries)

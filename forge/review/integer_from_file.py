from forge.review.files import get_file_data_list
from forge.review.filter import find_integer_after_pattern

"""
Retrieve integer after a pattern from a file in .data folder
"""

data = get_file_data_list("vip-file.csv")
pattern = "'id': "
objs = []

for line in data:
    if pattern in line:
        objs.append(find_integer_after_pattern(line, pattern))

print(f"Events received: {len(objs)}")
print(objs)

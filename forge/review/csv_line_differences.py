from forge.review.files import get_files
from forge.review.compare_files import differences_between_files
from forge.review.csv_operations import retrieve_data_set

"""
Reads files inside .data folder, if there are 2 csv files compares all lines present
If there are more or less than 2 csv files exists gracefully
"""

filenames = get_files("*.csv", 2)
filenames.sort()
files = []

for filename in filenames:
    files.append(retrieve_data_set(filename))

# for index in range(len(files)):
#     print(f"File {filenames[index]} contained {len(files[index])} line(s)")

differences = differences_between_files(files)

if not differences:
    print(f"No differences where found between files")
else:
    print(f"The following differences where found in both files: \n {differences}")

import csv
from os.path import exists


def create_sorted_csv(filename: str, filter_argument: str, filter_header: str):
    data = retrieve_data_set(filename)
    header = get_header(filter_header, data)
    sorted_data = sort_data(filter_header, header, data)
    if not sorted_data:
        print("Data from {} was empty, no file was created!".format(filename))
    else:
        write_to_csv(header, sorted_data, generate_filename(filter_argument))


def create_sorted_filtered_csv(filename: str, element: str, header_argument: str):
    header, data = filter_by_element(filename, element, header_argument)
    sorted_data = sort_data(header_argument, header, data)

    if not sorted_data:
        print("Data from {} was empty, no file was created!".format(filename))
    else:
        write_to_csv(header, sorted_data, generate_filename(element))


def create_differences_csv(first_filename, second_filename):
    diff = differences_between(first_filename, second_filename)
    if not diff:
        print(
            "No differences were found between {} and {}, no file was created!".format(
                first_filename, second_filename
            )
        )
    else:
        write_to_csv("", diff, generate_filename("differences", "-"))


def write_to_csv(header: list, entries: list, filename: str):
    # print(f"header: {header[:10]}, entries {entries[:10]}, filename {filename}")
    with open(filename, "x", newline="", encoding="utf-8") as file:
        output = csv.writer(file)
        output.writerow(header)
        output.writerows(entries)


def to_set(data) -> set:
    return set(map(tuple, data))


def sort_by(argument, data):
    return sorted(data, key=lambda x: x[argument])


def retrieve_data_set(filename: str) -> set:
    with open(filename, "rt", newline="", encoding="utf-8") as file:
        data = csv.reader(file)
        return to_set(data)


def generate_filename(argument: str, separator: str = "-history-"):
    filename = ""
    for index in range(1, 10):
        filename = "".join(
            ["bi-", argument.lower(), separator.lower(), str(index), ".csv"]
        )
        if not exists(filename):
            break

    return filename


def get_header(element: str, data: set):
    for row in data:
        if element in row:
            header = row
            data.remove(row)
            return header


def sort_data(filter_header, header, data):
    header_list = list(header)
    index = header_list.index(filter_header)
    return sort_by(index, data)


def filter_by_element(filename: str, element: str, header_argument: str):
    data = retrieve_data_set(filename)
    header = get_header(header_argument, data)

    filtered_data = set()
    for row in data:
        if element in row:
            filtered_data.add(row)
    return header, filtered_data


def differences_between(first_filename, second_filename):
    file_one_data = retrieve_data_set(first_filename)
    file_two_data = retrieve_data_set(second_filename)

    return file_one_data.symmetric_difference(file_two_data)

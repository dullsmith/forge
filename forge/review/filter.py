import re


def find_integer_after_pattern(line: str, pattern: str):
    if pattern not in line:
        return None
    int_list = integers_after_pattern(line, pattern)
    if len(int_list) == 0:
        return None
    else:
        return int_list[0]


def integers_after_pattern(line: str, pattern: str) -> list:
    f"""
    Split the line starting from {pattern} until the end of the line,
    find the next possible integer.
    [-1] indexes the array from the end.
    :param line: source string to perform actions
    :param pattern: pattern used to break teh string
    :return: str of an integer - if pattern is not there returns first
    """
    substr = line.split(pattern)[-1]
    return re.findall(r"\d+", substr)


def find_all_ints_between_patterns(
    pattern_one: str, pattern_two: str, line: str
) -> list:
    # Select lines between two patterns
    substr = line.split(pattern_one)[-1]
    trimmed_str = substr.split(pattern_two)[0]
    return re.findall(r"\d+", trimmed_str)

import os
import time

import requests
from dotenv import load_dotenv

load_dotenv()

API_PATH = os.getenv("API_PATH")
LOCALHOST = "http://localhost:8080/"
url = f"{LOCALHOST}{API_PATH}"

print(url)

response = requests.get(url)
response_time_in_seconds = response.elapsed.total_seconds()

gmt_time = time.gmtime(response_time_in_seconds)
human_readable_time = time.strftime("%H:%M:%S", gmt_time)

print(f"Response: {response.content}")
print(f"It took: {human_readable_time}")

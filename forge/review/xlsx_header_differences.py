import warnings

import pandas as pd

from forge.review.files import get_files

"""
Reads files inside .data folder, if there are 2 xlsx files compares the headers present
If there are more than 2 xlsx files exists gracefully
"""

filenames = get_files("*.xlsx", 2)

headers = []
for filename in filenames:
    """
    Warning filter used to silence pandas complains
    UserWarning: Workbook contains no default style, apply openpyxls default
    warn("Workbook contains no default style, apply openpyxls default")
    """
    warnings.simplefilter("ignore")
    df = pd.read_excel(filename)
    headers.append(df.axes[1])

file_one_headers = headers[0]
file_two_headers = headers[1]
differences = file_one_headers.symmetric_difference(file_two_headers).tolist()

if not differences:
    print(f"No differences where found between files")
else:
    print(f"The following headers were not found in both files: \n {differences}")

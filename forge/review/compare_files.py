from forge.review.files import get_files


def diff(file_one: set, file_two: set):
    return file_one.symmetric_difference(file_two)


def file_contains(line: str, file: list):
    if line not in file:
        return line


def diff_per_file(file_one: list, file_one_ref: int, file_two: list, file_two_ref: int):
    lines_not_present_in_file_two = []
    for line in file_one:
        file_one_comparison = file_contains(line, file_two)
        if file_one_comparison is not None:
            lines_not_present_in_file_two.append(file_one_comparison)
    print(f"Lines of file {file_one_ref} not present in {file_two_ref}:")
    print(f"{lines_not_present_in_file_two}")

    lines_not_present_in_file_one = []
    for line in file_two:
        file_two_comparison = file_contains(line, file_one)
        if file_two_comparison is not None:
            lines_not_present_in_file_one.append(file_two_comparison)
    print(f"Lines of file {file_two_ref} not present in {file_one_ref}:")
    print(f"{lines_not_present_in_file_one}")


def differences_between_files(file_list: list):
    differences_list = []
    for list_index in range(len(file_list)):
        for list_pivot in range(list_index + 1, len(file_list)):
            differences = diff(set(file_list[list_index]), set(file_list[list_pivot]))
            # print(f"Differences between file {list_index} and {list_pivot} = {len(differences)} line(s)")
            # print(*differences)
            differences_list.append(differences)
    return differences_list


def print_file_lines_size(file_list: list):
    for index in range(len(file_list)):
        print(f"File {index} has {len(file_list[index])}")


filenames = get_files("*.csv", None)
filenames.sort()
files = []

for filename in filenames:
    with open(filename, "rt") as open_file:
        lines = open_file.readlines()
        files.append(lines)

print_file_lines_size(files)

differences_between_files(files)

for file_index in range(len(files)):
    for file_pivot in range(file_index + 1, len(files)):
        diff_per_file(files[file_index], file_index, files[file_pivot], file_pivot)

# TODO: Break this in scratch and operations

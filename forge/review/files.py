import glob
import sys

from dotenv import load_dotenv

from forge.settings import MODULE_DIR

load_dotenv()

SRC_PATH = MODULE_DIR


def filepath(path_predicate: str, full_path: bool):
    f"""
    Finds all file paths matching the given {path_predicate} making use of {SRC_PATH} and .data folder
    :param full_path: in case full path is True {path_predicate} is the full path,
    otherwise is a pattern to add into {SRC_PATH}
    :param path_predicate: filename, can contain wildcards, or folders, or be full path
    :return: list of file paths - can be empty
    """

    if full_path:
        data_path = path_predicate
    else:
        data_path = f"{SRC_PATH}/.data/{path_predicate}"

    print(f"File path: {data_path}")
    return glob.glob(data_path)


def get_files(identifier: str, num: int = None, full_path: bool = False):
    files = filepath(identifier, full_path)

    if len(files) == 0:
        sys.exit(f"No files were found matching the identifier {identifier}")
    if num is not None and len(files) != num:
        sys.exit(
            f"Required {num} file(s) but found {len(files)} file(s) for {identifier}"
        )

    # print(csv_files)
    if num == 1:
        return files[0]
    else:
        return files[:num]


def get_file_data_list(identifier: str) -> list:
    file = get_files(identifier, 1)
    return line_list(file)


def line_list(filename: str) -> list:
    f"""
    Opens a file by given {filename} and returns data as list
    :param filename: File path
    :return: list containing lines from the file
    """
    with open(filename, "rt") as data:
        return list(data)

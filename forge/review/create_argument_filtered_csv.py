import sys

from forge.review.files import get_files
from forge.review.csv_operations import create_sorted_filtered_csv

if len(sys.argv) != 2:
    sys.exit(
        f"Unexpected arguments found, {len(sys.argv)} arguments were found, 2 were expected."
    )

filter_argument = sys.argv[1]

input_files = get_files("bi-part*.csv", 4)
print(input_files)

print("Loading files...")
for file in input_files:
    create_sorted_filtered_csv(file, filter_argument, "UTC_Time")
print("Ending operation...")

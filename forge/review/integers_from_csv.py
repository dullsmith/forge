import re

from forge.review.files import get_files

"""
Retrieve matching source order id and target order id from a csv file in .data folder
"""

filename = get_files("some-vip-file-received-1.csv", 1)

lines = []
first_filter = "orderId="
second_filter = "targetOrderId="

with open(filename, "rt") as my_file:
    for line in my_file:
        if first_filter in line:
            pair = {}
            # Split the line by string - [-1] indexes the array from the end
            first_substr = line.split(first_filter)[-1]
            pair[first_filter] = re.findall(r"\d+", first_substr)[0]
            second_substr = line.split(second_filter)[-1]
            pair[second_filter] = re.findall(r"\d+", second_substr)[0]
            lines.append(pair)

# for entry in lines:
#     print(entry)

unique_lines = []
copies = []
for item in lines:
    if item not in unique_lines:
        unique_lines.append(item)
    else:
        copies.append(item)

print(f"Events received: {len(lines)}")
print(f"Events received: {len(unique_lines)}")
for copy in copies:
    print(copy)

import os

import requests as requests
from dotenv import load_dotenv

load_dotenv()
url = os.getenv("api-url")
print(url)

list_ids = [1111111, 1111112, 1111113]


def get_response(url: str, parameters: str):
    # print(f"{url}{parameters}")
    response = requests.get(f"{url}{parameters}")
    assert response.status_code == 200
    return response.json()


for some_id in list_ids:
    resp = get_response(url, str(some_id))
    print(f"{resp['identifier']} - {resp['some_other_identifier']}")

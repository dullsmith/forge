import random
import re

from forge.review.csv import write_single_column_to_csv
from forge.review.files import get_files, line_list

"""
Open multiple files, take 20 examples randomly, remove duplicates and create a file
"""

filenames = get_files("/some/very/big/filename.csv", full_path=True)
random_events = []

for file in filenames:
    fl = line_list(file)
    random_list = random.sample(range(1, len(fl)), 500)
    for index in random_list:
        integer = re.findall(r"\d+", fl[index])[0]
        random_events.append(int(integer))

write_single_column_to_csv(
    "transaction id", random_events, "../.data/random_file_prod.csv"
)

print(f"Events received: {len(random_events)}")
print(random_events)

# Makefile

checkmark=\xE2\x9C\x94  # Unicode character representation
warning=\xE2\x9A\xA0  # Unicode character representation

define warn
	@tput bold
	@tput setaf 3
	@printf "${warning}${1}\n"
	@tput sgr0
endef

define log
	@tput bold
	@tput setaf 6
	@printf "${checkmark}${1}\n"
	@tput sgr0
endef

.DEFAULT_GOAL := help
ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
VENV := .venv

###############################################################################
# Default
###############################################################################

.PHONY: help
help:
	@echo "|> Directory: ${ROOT_DIR}"
	@echo "|> OS: ${OS}"
	@echo "|> Available targets:"
	@make -qpRr | grep -E '^[a-z].*:$$' | sed -e 's~:~~g' | sort

.PHONY: all
all: clean install
	@$(call log, all)

.PHONY: install
install: poetry-setup pre-commit-setup
	@$(call log, setup)

.PHONY: update
update: poetry-update pre-commit-update
	@$(call log, update)

.PHONY: test
test: poetry-test
	@$(call log, test)

.PHONY: clean
clean: delete-venv
	@$(call log, clean venv)

###############################################################################
# Setup
###############################################################################

.PHONY: clone-foundry
clone-foundry:
	@$(call warn, set git repositories, requires proper ssh configuration)
	poetry run bin/clone_repositories.py -s ./foundry.yaml -r

.PHONY: update-foundry
update-foundry:
	@$(call warn, update foundry repositories)
	@poetry run bin/updategitrepos.py \
		--foundry \
		--ignore $(HOME)/foundry/depository/archived $(HOME)/foundry/depository/demos/ \
		-r

###############################################################################
# Poetry setup and help
###############################################################################

.PHONY: poetry-setup
poetry-setup: poetry-local delete-venv poetry-install

.PHONY: poetry-install
poetry-install:
	@$(call warn, create and install poetry dependencies)
	poetry install

.PHONY: delete-venv
delete-venv:
	@$(call warn, delete existent virtualenv)
	rm -rf $(VENV)
	rm -rf venv

.PHONY: poetry-local
poetry-local:
	@$(call warn, setting virtualenv location to project)
	poetry config virtualenvs.in-project true

.PHONY: poetry-env
poetry-env:
	@$(call warn, get poetry environment)
	poetry env info --path

.PHONY: poetry-update
poetry-update:
	@$(call warn, update poetry dependencies)
	poetry update
	poetry self update

.PHONY: poetry-test
poetry-test:
	@$(call warn, run tests with poetry)
	poetry run pytest

###############################################################################
# Pre commit
###############################################################################

.PHONY: pre-commit
pre-commit: pre-commit-setup pre-commit-update

.PHONY: pre-commit-setup
pre-commit-setup:
	@$(call warn, install pre-commit dependencies)
	pre-commit install

.PHONY: pre-commit-update
pre-commit-update:
	@$(call warn, update pre-commit dependencies)
	pre-commit autoupdate

###############################################################################
# Commitizen
###############################################################################

.PHONY: bump
bump:
	@$(call warn, bump version of commitizen)
	cz bump
	@$(call log, sem ver bumped)

.PHONY: bump-and-push
bump-and-push: bump
	@$(call warn, push git with tags)
	git push
	git push --tags

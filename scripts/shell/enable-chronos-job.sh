#!/bin/bash

# Depends on JQ
#
# netrc configuration required.
# https://everything.curl.dev/usingcurl/netrc
#
# `.netrc` file example
#
# machine chronos.dev.net
# login user
# password pass
#
# machine marathon.dev.net
# login user
# password pass

# CURL credentials (-n)
#        --netrc-file <filename>
#              This option is similar to -n, --netrc, except that you
#              provide the path (absolute or relative) to the netrc file
#              that curl should use. You can only specify one netrc file
#              per invocation. (...)

CHRONOS_ENDPOINT="https://chronos.dev.net"
PREFIX="job_path_chronos"

JOB=$(curl -n -s $CHRONOS_ENDPOINT/scheduler/jobs | jq -r '.[] | select(.name | startswith("'$PREFIX'")) | .name')

# Get job definition and modify it to set "disabled": false
curl -n -s $CHRONOS_ENDPOINT/scheduler/jobs | jq -r '.[] | select(.name | startswith("'$JOB'"))' | jq '.disabled = false' >$job.json

# Recreate the job with the updated definition
curl -n -s -X PUT -H "Content-Type: application/json" $CHRONOS_ENDPOINT/scheduler/iso8601 -d @"$JOB".json
echo "Enabled job: $job"
